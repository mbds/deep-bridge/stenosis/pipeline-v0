import torch as torch
import torch.nn as nn
import torch.optim as optim
from medicaltorch import models as mt_models
from medicaltorch import losses as mt_losses
from medicaltorch import metrics as mt_metrics
import numpy as np

def evaluate_model(model, val_loader, epoch, num_epochs, writer, current_lr, log_every=20):
    _ = model.eval()
    
    if torch.cuda.is_available():
        model.cuda()
        
    losses = []

    for i, batch in enumerate(val_loader):
        image, mask = batch["input"], batch["gt"]
        
        if torch.cuda.is_available():
            image = image.cuda()
            mask = mask.cuda(non_blocking=True)


        if model == mt_models.Unet(drop_rate=0.4, bn_momentum=0.1):
            prediction = model(image)
        else :
            prediction = model.forward(image)

        loss = mt_losses.dice_loss(prediction, mask)
        loss_value = loss.item()
        losses.append(loss_value)

        
        mask_npy = mask.cpu().numpy().astype(np.uint8)
        mask_npy = mask_npy.squeeze(axis=1)

        prediction = prediction.data.cpu().numpy().astype(np.uint8)
        prediction = prediction.squeeze(axis=1)


        try:
            acc =  mt_metrics.accuracy_score(prediction, mask_npy)
        except:
            acc = 0.5



        writer.add_scalar('Val/Loss', loss_value,
                          epoch * len(val_loader) + i)
        writer.add_scalar('Val/ACC', acc, epoch * len(val_loader) + i)

        if (i % log_every == 0) & (i > 0):
            print('''[Epoch: {0} / {1} |Single batch number : {2} / {3} ]| avg val loss {4} | val acc : {5} | lr : {6}'''.
                  format(
                      epoch + 1,
                      num_epochs,
                      i,
                      len(val_loader),
                      np.round(np.mean(losses), 4),
                      np.round(acc, 4),
                      current_lr
                  )
                  )

    writer.add_scalar('Val/ACC_epoch', acc, epoch + i)

    val_loss_epoch = np.round(np.mean(losses), 4)
    val_perf_epoch = np.round(acc, 4)
    return val_loss_epoch, val_perf_epoch