import torch
from medicaltorch import datasets as mt_datasets
from medicaltorch import filters as mt_filters

def get_loaders(directory, train_transform, val_transform, batch):
    from medicaltorch import datasets as mt_datasets
    gmdataset_train = mt_datasets.SCGMChallenge2DTrain(root_dir=directory,
                                                       subj_ids=range(1, 9),
                                                       transform=train_transform,
                                                       slice_filter_fn=mt_filters.SliceFilter())

    gmdataset_val = mt_datasets.SCGMChallenge2DTrain(root_dir=directory,
                                                     subj_ids=range(9, 11),
                                                     transform=val_transform)
                                                 
    train_loader = torch.utils.data.DataLoader(gmdataset_train, 
                                               batch_size=batch, 
                                               shuffle=True, pin_memory=True, 
                                               collate_fn=mt_datasets.mt_collate, 
                                               num_workers=1)

    val_loader = torch.utils.data.DataLoader(gmdataset_val, 
                                             batch_size=batch, 
                                             shuffle=True, pin_memory=True, 
                                             collate_fn=mt_datasets.mt_collate, 
                                             num_workers=1)

    return train_loader, val_loader