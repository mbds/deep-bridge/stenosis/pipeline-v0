class Load_data(data.Dataset):
    def __init__(self, root_dir, form, train=True, transform=None):
        super().__init__()
        self.root_dir = root_dir
        self.train = train
        self.form = form
        if self.train:
            self.folder_path = self.root_dir + 'train/'
            self.records = pd.read_csv(
                self.root_dir + 'train.csv', header=None, names=['id'])
        else:
            self.folder_path = self.root_dir + 'valid/'
            self.records = pd.read_csv(
                self.root_dir + 'valid.csv', header=None, names=['id'])
          
        self.img_paths = [self.folder_path + filename + '-image' + 
                      form for filename in self.records['id'].tolist()]
        self.mask_paths = [self.folder_path + filename + '-mask-r1' + 
                      form for filename in self.records['id'].tolist()]

        self.transform = transform

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, index):
    
    	if self.form=='.nii.gz':
    		img = nib.load(self.img_paths[index])
    		msk = nib.load(self.mask_paths[index])
            image = img.get_fdata()
            mask = msk.get_fdata()
    	else :
        	image = np.load(self.img_paths[index], allow_pickle=True)
			mask = np.load(self.mask_paths[index], allow_pickle=True)
        
        if self.transform:
            image = self.transform(image)
            mask = self.transform(mask)
        else:
            image = np.stack((image,)*3, axis=1)
            image = torch.FloatTensor(image)
            mask = np.stack((mask,)*3, axis=1)
            mask = torch.FloatTensor(mask)
        
        return image, mask