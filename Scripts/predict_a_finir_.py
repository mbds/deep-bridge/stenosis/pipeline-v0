import shutil
import os
import time
from datetime import datetime
import argparse
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import models
from torch.autograd import Variable
import torch.nn.functional as F

from tensorboardX import SummaryWriter

from medicaltorch import models as mt_models
from sklearn import metrics


def predict(task, model, train_loader):
    _ = model.train()

    if torch.cuda.is_available():
        model.cuda()
        image = image.cuda()
    
    prediction = model(image)
    
    if task == 'segmentation':
    	prediction = prediction.data.cpu().numpy().astype(np.uint8)
    	prediction = prediction.squeeze(axis=1)
    	#np.savetxt('./predictions/output.csv', arr)
    	
    elif task == 'classification':
    	prediction = torch.sigmoid(prediction).item()
    
    return prediction


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--task', type=str, required=True,
                        choices=['segmentation', 'classification'])
    parser.add_argument('--data_directory', type=str, required=True)
    parser.add_argument('--model_directory', type=str, required=True)
    parser.add_argument('--type', type=str, required=True,
                        choices=['.npy', '.nii.gz'])
    parser.add_argument('--transforms', type=str, required=True, 
    					choices=['transform_classif', 'transform_seg'])
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_arguments()

	if args.type == '.npy':
		image = np.load(args.data_directory, allow_pickle=True)
	elif args.type == '.nii.gz':
		image = nib.load(args.data_directory)
		image = image.get_fdata()
			
	if args.transforms == 'transform_seg':
		from Transform_seg import get_transforms
		train_transforms, test_transforms = get_transforms()	
    	image = test_transforms(image)

	model=torch.load(args.model_directory, map_location=torch.device('cpu'))
			
    predict(args.task, model, image)
