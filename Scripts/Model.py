import shutil
import os
import time
from datetime import datetime
import argparse
import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import models
from torch.autograd import Variable
import torch.nn.functional as F

from tensorboardX import SummaryWriter

from medicaltorch import models as mt_models
from sklearn import metrics


def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']
        

def run(task, model, train_loader, validation_loader, prefix_name, batch, epochs, lr, flush_history, save_model, patience, log_every):

    if task=='classification':
        from Train_classif import train_model as train_model
        from Evaluate_classif import evaluate_model as evaluate_model

    elif task=='segmentation':
        from Train_seg import train_model as train_model
        from Evaluate_seg import evaluate_model as evaluate_model
        
    log_root_folder = "./logs/"
    if flush_history == 1:
        objects = os.listdir(log_root_folder)
        for f in objects:
            if os.path.isdir(log_root_folder + f):
                shutil.rmtree(log_root_folder + f)
    now = datetime.now()
    logdir = log_root_folder + now.strftime("%Y%m%d-%H%M%S") + "/"
    os.makedirs(logdir)
    writer = SummaryWriter(logdir)

    if torch.cuda.is_available():
        model = model.cuda()

    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=0.1)

    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=3, factor=.3, threshold=1e-4, verbose=True)

    best_val_loss = float('inf')
    best_val_perf = float(0)

    num_epochs = epochs
    iteration_change_loss = 0

    t_start_training = time.time()

    for epoch in range(num_epochs):
        current_lr = get_lr(optimizer)

        t_start = time.time()
        
        train_loss, train_perf = train_model(
            model, train_loader, epoch, num_epochs, optimizer, writer, current_lr, log_every)
        val_loss, val_perf = evaluate_model(
            model, validation_loader, epoch, num_epochs, writer, current_lr, log_every)
        
        scheduler.step(val_loss)

        t_end = time.time()
        delta = t_end - t_start

        print("train loss : {0} | train perf {1} | val loss {2} | val perf {3} | elapsed time {4} s".format(
            train_loss, train_perf, val_loss, val_perf, delta))

        iteration_change_loss += 1
        print('-' * 30)

        if val_perf > best_val_perf:
            best_val_perf = val_perf
            if bool(save_model):
                file_name = f'model_{prefix_name}_val_perf_{val_perf:0.4f}_epoch_{epoch+1}.pth'
                for f in os.listdir('./models/'):
                        os.remove(f'./models/{f}')
                torch.save(model, f'./models/{file_name}')

        if val_loss < best_val_loss:
            best_val_loss = val_loss
            iteration_change_loss = 0

        if iteration_change_loss == patience:
            print('Early stopping after {0} iterations without the decrease of the val loss'.
                  format(iteration_change_loss))
            break

    t_end_training = time.time()
    print(f'training took {t_end_training - t_start_training} s')


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--task', type=str, required=True,
                        choices=['segmentation', 'classification'])
    parser.add_argument('-d', '--directory', type=str, required=True)
    parser.add_argument('--type', type=str, required=True,
                        choices=['.npy', '.nii.gz'])
    parser.add_argument('--prefix_name', type=str, required=True)
    parser.add_argument('--transforms', type=str, required=True, 
    					choices=['transform_classif', 'transform_seg'])
    parser.add_argument('--seg_method', type=str, default='unet', 
    					choices=['unet', 'attentionUnet'])
    parser.add_argument('--classif_method', type=str, default='alexNet', 
    					choices=['alexNet', 'MRnet'])
    parser.add_argument('--batch', type=int, default=1)
    parser.add_argument('--epochs', type=int, default=50)
    parser.add_argument('--lr', type=float, default=1e-5)
    parser.add_argument('--flush_history', type=int, choices=[0, 1], default=0)
    parser.add_argument('--save_model', type=int, choices=[0, 1], default=1)
    parser.add_argument('--patience', type=int, default=5)
    parser.add_argument('--log_every', type=int, default=100)
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_arguments()

	if args.transforms ==  'transform_classif':		
		from Transform_classif import get_transforms
		transforms = get_transforms()
	
	elif args.transforms == 'transform_seg':
		from Transform_seg import get_transforms
		train_transforms, validation_transforms = get_transforms()	
    
  if args.task=='classification':
		from Load_data_classif import get_loaders
		
		train_loader, validation_loader = get_loaders(args.directory, args.type, transforms, args.batch)
		
		if args.classif_method == 'MRnet':
			import MRNet
			model = MRNet.MRNet()
		else:
			model = models.alexnet(pretrained=True)
			
	elif args.task=='segmentation':		
		from Load_data_seg import get_loaders
		
		train_loader, validation_loader = get_loaders(args.directory, train_transforms, validation_transforms, args.batch)
    	
    	if args.seg_method == 'attentionUnet':
			import AttentionUnet
			model = AttentionUnet.UNet_Attention()
		else:
			model = mt_models.Unet(drop_rate=0.4, bn_momentum=0.1)
			
    run(task, model, train_loader, validation_loader, args.prefix_name, args.batch, args.epochs, args.lr, args.flush_history,
    	args.save_model, args.patience, args.log_every)
