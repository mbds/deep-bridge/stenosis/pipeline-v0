import torch 
from Read_data_classif import Read_data

def get_loaders(directory, type, transform, batch):
    from Read_data_classif import Read_data
    train_dataset = Read_data(directory, type, transform=transform, train=True)
    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=batch, shuffle=True, num_workers=11, drop_last=False)

    validation_dataset = Read_data(directory, type, train=False)
    validation_loader = torch.utils.data.DataLoader(
        validation_dataset, batch_size=batch, shuffle=-True, num_workers=11, drop_last=False)
    
    return train_loader, validation_loader