import os
import pandas as pd
import numpy as np

import torch
import torch.nn.functional as F
import torchvision.transforms.functional as TF
import torch.utils.data as data
from torchvision import transforms
import nibabel as nib



class Read_data(data.Dataset):
    def __init__(self, root_dir, form, train=True, transform=None, weights=None):
        super().__init__()
        self.root_dir = root_dir
        self.train = train
        self.form = form
        if self.train:
            self.folder_path = self.root_dir + 'train/'
            self.records = pd.read_csv(
                self.root_dir + 'train.csv', header=None, names=['id', 'label'])
        else:
            self.folder_path = self.root_dir + 'valid/'
            self.records = pd.read_csv(
                self.root_dir + 'valid.csv', header=None, names=['id', 'label'])

        self.records['id'] = self.records['id'].map(
            lambda i: '0' * (4 - len(str(i))) + str(i))
        self.paths = [self.folder_path + filename +
                      form for filename in self.records['id'].tolist()]
        self.labels = self.records['label'].tolist()

        self.transform = transform
        if weights is None:
            pos = np.sum(self.labels)
            neg = len(self.labels) - pos
            self.weights = [1, neg / pos]
        else:
            self.weights = weights

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, index):
    
        if self.form=='.nii.gz':
            img = nib.load(self.paths[index])
            array = img.get_fdata()
        else :
            array = np.load(self.paths[index], allow_pickle=True)
        
        label = self.labels[index]
        label = torch.FloatTensor([label])
        
        if self.transform:
            array = self.transform(array)
        else:
            array = np.stack((array,)*3, axis=1)
            array = torch.FloatTensor(array)

        if label.item() == 1:
            weight = np.array([self.weights[1]])
            weight = torch.FloatTensor(weight)
        else:
            weight = np.array([self.weights[0]])
            weight = torch.FloatTensor(weight)

        return array, label, weight