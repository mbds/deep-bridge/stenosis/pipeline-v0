import torch as torch
import torch.nn as nn
import torch.optim as optim
from medicaltorch import models as mt_models
from medicaltorch import losses as mt_losses
from medicaltorch import metrics as mt_metrics
import numpy as np

def train_model(model, train_loader, epoch, num_epochs, optimizer, writer, current_lr, log_every=100):
    _ = model.train()

    if torch.cuda.is_available():
        model.cuda()
    
    losses = []

    for i, batch in enumerate(train_loader):
        image, mask = batch["input"], batch["gt"]
        optimizer.zero_grad()
        
        if torch.cuda.is_available():
        	image = image.cuda()
        	mask = mask.cuda(non_blocking=True)

        if model == mt_models.Unet(drop_rate=0.4, bn_momentum=0.1):
            prediction = model(image)
        else:
            prediction = model.forward(image)

        loss = mt_losses.dice_loss(prediction, mask)
        loss.backward()
        optimizer.step()

        loss_value = loss.item()
        losses.append(loss_value)
       
        mask_npy = mask.cpu().numpy().astype(np.uint8)
        mask_npy = mask_npy.squeeze(axis=1)

        prediction = prediction.data.cpu().numpy().astype(np.uint8)
        prediction = prediction.squeeze(axis=1)

        try:
            acc =  mt_metrics.accuracy_score(prediction, mask_npy)
        except:
            acc = 0.5


        writer.add_scalar('Train/Loss', loss_value,
                          epoch * len(train_loader) + i)
        writer.add_scalar('Train/ACC', acc, epoch * len(train_loader) + i)

        if (i % log_every == 0) & (i > 0):
            print('''[Epoch: {0} / {1} |Single batch number : {2} / {3} ]| avg train loss {4} | train acc : {5} | lr : {6}'''.
                  format(
                      epoch + 1,
                      num_epochs,
                      i,
                      len(train_loader),
                      np.round(np.mean(losses), 4),
                      np.round(acc, 4),
                      current_lr
                  )
                  )

    writer.add_scalar('Train/ACC_epoch', acc, epoch + i)

    train_loss_epoch = np.round(np.mean(losses), 4)
    train_perf_epoch = np.round(acc, 4)
    return train_loss_epoch, train_perf_epoch