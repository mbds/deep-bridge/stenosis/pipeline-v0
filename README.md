# Guide et prérequis 

Nous avons développé une version 0 du pipeline que nous avons concu avec le groupe DEEP BRIDGE 2 qui permet de faire de la 
segmentation et de la classification d'images médicales.

Tandis que le groupe 2 a testé notre pipeline sur une tache de classification, nous l'avons testé en appliquant une tache de 
segmentation sur le jeu de données publiques *GM Spinal Cord Challenge Dataset* ([à telecharger ici](http://cmictig.cs.ucl.ac.uk/niftyweb/challenge/ ))

Pour tester nos programmes il faut:
- Avoir une version de Python à jour (> Python 3.6)
- Avoir `pip` installé
- Exécuter le fichier *install.py* depuis un terminal ou un cmd pour télécharger les librairies utiles.

# Implémentation du pipeline v0

Pour la v0, le pipeline est implémenté sous forme d'un programme "main" qui effetue des imports conditionnels selon des 
paramètres entrées par l'utilisateur.

Le répértoire ***scripts*** de ce projet contient:
- Deux modules pour lire des données de classification et de segmentation
- Deux modules pour appliquer une suite de transformations a des données de classification, et de segmentation
- Deux modules pour créer des dataloaders
- Un module pour créer un réseau de neurones *Attention U-net*
- Un module pour créer un réseau de neurones *MRNet*
- Deux modules pour entrainer des modèles 
- Deux modules pour évaluer des modèles
- *model.py*: c'est le programme "main" qui crée le modèle, il importe les modules précédents selon des paramètres à entrer par l'utilisateur
- *predict.py*: apres avoir crée un modèle avec *model.py*, on peut l'appliquer avec *predict.py*

Paramètres de *model.py*:
- `task`: *segmentation* ou *classification*
- `directory` : chemin du jeu de données d'apprentissage
- `type` : format des données, pour le moment *.npy* ou *.nii.gz* (nifti)
- `prefix_name` : nom à donner au modèle qui sera sauvegardé
- `transforms` : *transforms_seg* ou *transforms_classif*
- `seg_method` : *unet* ou *attentionUnet*
- `classif_method` : *alexNet* ou *MRnet*
- `batch` : nombre de batch 
- `epochs` : nombre d'epochs
- `lr` : learning rate initial
- `flush_history`: 0 ou 1, si 1 supprime tous les logs si un dossier logs est dèja existant
- `save_model` : 0 ou 1, si 1 le modèle crée est sauvé
- `patience` : condition d'arret
- `log_every` : l'ajout de ce parametre permet de garder un oeil sur l'avancement de l'apprentissage

Pour exécuter depuis un terminal ou un cmd (il faut que tous les scripts se trouvent au meme endroit):
```
python model.py --task 'segmentation' --directory 'chemin/vers/le/dataset/dapprentissage' --type 'nii.gz' 
--prefix_name 'testsegm' --transforms 'transforms_seg' --seg_method 'unet' --flush_history 1 --save_model 1
```

# Modeles crées

Le repertoire ***logs*** contient un fichier de logs du modèle. Pendant l'entrainement, les mesures d'évaluation du modèle 
pour chaque batch et chaque epoch sont écrites dans ce fichier. Pour observer les performances du modèle il faut:
- avoir tensorboard installé, ou pour l'installer, dans un terminal ou cmd: `pip install tensorboard`
- exécuter dans un terminal ou cmd : `tensorboard --logdir='chemin/vers/le/fichier/de/logs'`

Le répertoire ***models*** contient le model enregistré sous forme de dictionnaire. *Update : le modele est trop volumineux pour etre chargé.*

# Tests des modèles

Les fichiers *test_classif.ipynb* et *test_seg.ipynb* sont des notebooks jupyter, qui reprennent les modules et les codes
précédent pour les tester de facon simplifié et interractive (mais long à entrainer). Celui qui nous concerne est *test_seg.ipynb*, pour l'explorer:
- upload le jeu de données ainsi que tous les scripts .py sur son drive Google
- télécharger et ouvrir le notebook avec Google Colab
- mount son drive (cf. dans le notebook)
- se placer dans le repertoire ou se trouvent les scripts
- have fun!

